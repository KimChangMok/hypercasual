﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CircleChecker : Charactor
{
  //コントローラーのデルタを保管する場所
  List<Vector2> deltas;

  private void Awake()
  {
    deltas = new List<Vector2>();
  }
  private void Update()
  {
    if(controller.but0)
    {
      deltas.Add(controller.deltaVector);
    }
    else
    {
      Check_Circle();
      deltas.Clear();
    }
  }

  //丸を書いたのか確認
  private bool Check_Circle()
  {
    if(deltas.Count ==0)
    {
      return false;
    }

    //全デルタを足してみてどうなるかテスト
    Vector2 all = new Vector2(0,0);
    deltas.ForEach(x => all += x.normalized);

    Debug.Log(all);
    return true;
  }

}
