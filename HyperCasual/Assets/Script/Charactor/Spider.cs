﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;


//クモ自体獲物を狙って進んでいく
public class Spider : Charactor
{
  //えさのtransform
  Charactor target;

  float per;
  [SerializeField]
  float increase;

  private void Start()
  {
    per = 0.0f;
    Observable.FromCoroutine<bool>(observer => Chase(observer))
    .Subscribe(x =>
    {
      if(x)
      {
        Find_Target();
      }
    }).AddTo(gameObject);
  }

  private IEnumerator Chase(IObserver<bool> obsever)
  { 
    while (true)
    {
      //ターゲットが消えたらOnNext発行
      obsever.OnNext(target == null);

      //シーン内でえさが見つからない場合は探すまで繰り返す
      if(target == null)
      {
        yield return null;
        continue;
      }
      //相対距離を図る
      var my = transform.localPosition;
      var d = target.transform.localPosition - my;

      my += (d * per);

      //移動
      transform.localPosition = my;

      //比率上昇
      per += increase;

      yield return null;
    }
  }
  
  private void Find_Target()
  {
    var t = GameManager.instance.FindNextFeed(transform);
    if(t == null)
    {
      Debug.Log("Feed is Empty");
      return;
    }

    target = t;
    per = 0.0f;
  }

}