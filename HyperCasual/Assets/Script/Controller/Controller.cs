﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//全キャラクタの行動のフラグを発行する親クラス
public class Controller : MonoBehaviour
{
  public enum ControllerType
  {
    Unkown = -1,
    Touch = 0,            //プレイヤのコントローラー
    AI = 1,               //自動で状況判断するコントローラー
    Auto = 2,             //レコードプレイ又は、指定の行動だけを繰り返すコントローラー
    Mouse = 3,            //テスト用マウスコントローラー
  }

  //コントローラー識別用
  public ControllerType type { get; protected set; }

  //フラグチェックをUniRXでやるか、UpDateでやるか決めていない(2019/11/05)


  //外部収得用フラグたち  
  public bool up { get; protected set; }
  public bool down { get; protected set; }
  public bool left { get; protected set; }
  public bool right { get; protected set; }
  public bool but0 { get; protected set; }

  public Vector2 deltaVector { get; protected set; }
  //フラグ追加はここで

  //オーバーライド関数

  protected virtual void Check_Delta()
  {

  }
  protected virtual void Check_Button0()
  {

  }
}
