﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : Controller
{
  Vector3 before_Pos;
  Vector3 now_Pos;

  private void Awake()
  {
    before_Pos = Vector3.zero;
    now_Pos = Vector3.zero;
    type = ControllerType.Mouse;
  }


  private void Update()
  {
    if(Input.GetMouseButton(0))
    {
      now_Pos = Input.mousePosition;
    }
    else
    {
      now_Pos = Vector3.zero;
    }

    Check_Delta();
    Check_Button0();
    before_Pos = now_Pos;
  }

  protected override void Check_Button0()
  {
    but0 = Input.GetMouseButton(0);
  }

  protected override void Check_Delta()  
  {
    if(Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
    {
      return;
    }
    var d = now_Pos - before_Pos;    

    deltaVector = new Vector2(d.x, d.y);

    Update_Flag();
  }

  private void Update_Flag()
  {
    up = deltaVector.y > 0.0f;
    down = deltaVector.y < 0.0f;
    left = deltaVector.x < 0.0f;
    right = deltaVector.x > 0.0f;
  }

}
