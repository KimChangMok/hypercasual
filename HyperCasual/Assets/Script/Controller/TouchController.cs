﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

//タッチのコントローラー
public class TouchController : Controller
{
  private void Awake()
  {
    this.type = ControllerType.Touch;
  }

  //フラグ更新
  private void Update()
  {
    Check_Delta();


  }

  //上下左右チェック
  protected override void Check_Delta()
  {
#if !UNITY_EDITOR
    deltaVector = Input.GetTouch(0).deltaPosition;
#endif

    up = deltaVector.y > 0.0f;
    down = deltaVector.y < 0.0f;
    left = deltaVector.x < 0.0f;
    right = deltaVector.x > 0.0f;
  }
}
