﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


//クモが狙うえさたちのマネージャー
public class FeedManager : MonoBehaviour
{
  //えさの基本形のプレハブ(後でちゃんと作ること(2019/11/06))
  [SerializeField]
  private Charactor feed_Prefab;
  //インスペクターで見やすくするためにserializefield適用
  [SerializeField]
  private List<Charactor> feeds;

  private void Awake()
  {
    feeds = new List<Charactor>();
  }


  //外からのえさ生成関数(引数は個数)
  public void CreateFeeds(int num = 1)
  {
    //プレハブ設定がされていない場合
    if(feed_Prefab == null)
    {
      Debug.Log("is not set feed prefab");
      return;
    }
    for(int i = 0; i< num; i++)
    {
      //位置をランダム化して生成する
      var f = Instantiate(feed_Prefab, transform);

      Vector3 npos = new Vector3(Random.Range(-Screen.width / 2.0f, Screen.width / 2.0f), Random.Range(-Screen.height / 2.0f, Screen.height / 2.0f));
      f.transform.localPosition = npos;

      feeds.Add(f);
    }
  }

  //引数から一番近いえさを探してくれる
  public Charactor GetNearstFeed(Transform t)
  {
    if(feeds.Count == 0)
    {
      return null;
    }

    float best_Distance = float.MaxValue;
    int i = 0;

    //もし、消えているものがリストの中に残っていてもバグらないように
    foreach(var f in feeds.FindAll(x => x!=null))
    {
      //距離を図る
      var d = (f.transform.localPosition - t.localPosition).magnitude;
      //一番近いならば更新
      if(d < best_Distance)
      {
        best_Distance = d;
        i = feeds.FindIndex(x=> x.Equals(f));
      }
    }

    return feeds[i];
  }
}
