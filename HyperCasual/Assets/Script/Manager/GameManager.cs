﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

//ゲームマネージャー
public class GameManager : SingletonMonoBehaviour<GameManager>
{
  protected override void Awake()
  {
    base.Awake();
    DontDestroyOnLoad(gameObject);
  }

  //えさのマネージャー
  [SerializeField]
  FeedManager feedManager;


  private void Start()
  {
    GameInitialize();
  }


  public Charactor FindNextFeed(Transform t)
  {
    return feedManager.GetNearstFeed(t);
  }


  //ゲームの初期化処理
  //必要なオブジェクトとかを生成し、初期値を設定する
  public void GameInitialize()
  {
    feedManager.CreateFeeds(10);
  }

  //ゲームオーバー処理
  //ゲームオーバーが確定されてタイトルに戻るか、ダイアログを呼ぶかになる、もしくはリスタートの入力待ちになるかも
  public void GameOver()
  {    
    //未実装
  }


}
