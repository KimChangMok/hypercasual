public class Singleton<T> where T : class, new()
{
  private static volatile T mInstance = null;
  private static object     mSyncObj  = new object();

  protected Singleton() {}

  public static T instance {
    get  {
      if (mInstance == null) {
        lock(mSyncObj) {
          mInstance = new T();
        }
      }
      return mInstance;
    }
  }
}