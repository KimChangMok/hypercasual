﻿using UnityEngine;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
{
  protected static T mInstance;

  public static T instance
  {
    get
    {
      if (mInstance == null)
      {
        mInstance = (T)FindObjectOfType(typeof(T));
        if (mInstance == null)
        {
           Debug.LogWarning(typeof(T) + " is nothing");
        }
      }
      return mInstance;
    }
  }

  protected virtual void Awake()
  {
    checkInstance();
  }

  protected bool checkInstance()
  {
    if (mInstance == null)
    {
      mInstance = (T)this;
      return true;
    }
    else if (mInstance == this)
    {
      return true;
    }

    Destroy(this.gameObject);
    return false;
  }

  protected void OnDestroy()
  {
    if (mInstance == this)
    {
      mInstance = null;
    }
  }
}
